/*
 *	We use our own fseek because SVR4's seem to flush the stream
 *	unconditionally.  SVR4 is supposed for this package.
 *
 *						H.Nakano (94/03/23)
 */

extern FILE *xxfopen();
extern FILE *xxfreopen();
extern FILE *xxfdopen();
extern int xxfflush();
extern int xxfread();
extern int xxfgetc();
extern int xxgetw();
extern char *xxgets();
extern char *xxfgets();
extern int xxfseek();
extern long xxftell();
extern int xx__filbuf();

#define fopen		xxfopen
#define freopen		xxfreopen
#define fdopen		xxfdopen
#define fflush		xxfflush
#define fread		xxfread
#define fgetc		xxfgetc
#define getw		xxgetw
#define gets		xxgets
#define fgets		xxfgets
#define fseek		xxfseek
#define ftell		xxftell
#define __filbuf	xx__filbuf

