# The following file and directory specifications may need changing at
# your site:
#
# BINAREA and DVI2LIB are relative to PREFIX
PREFIX=/usr/local
# where dvi2dvi is installed
BINAREA=${PREFIX}/bin
#
# directory for dvi2dvi: where the auxiliary files are installed
DVI2LIB=${PREFIX}/lib/dvi2dvi
#
# name of default fontdesc file in your site
FONTDESC=fontdesc
#
DVI2PATH=.

# kpathsea
KPSELIB = -L${PREFIX}/lib -lkpathsea
KPSEINC = -I${PREFIX}/include -DKPATHSEA

#
CC=gcc
#
CCFLAGS = -g -DPOSIX
#CCFLAGS = -O		# SUN etc ...
#CCFLAGS = -O -traditional -DPOSIX	# 386bsd (gcc 1.X)
#CCFLAGS = -O -DPOSIX	# Netbsd (gcc 2.X), Linux
#CCFLAGS = -O -bsd	# NeXT
#CCFLAGS = -O -DMSDOS -DPOSIX	# DJGCC (gcc 2.X)
#CCFLAGS = -O -DSYSV	# SysV
#CCFLAGS = -O -DPOSIX -DSYSV -DSYSVIO	# Solaris 2.X (acc, gcc -- warnings)
#CCFLAGS = -O -DSYSVIO	# Solaris 2.X (ucb/cc -- warnings)

# BSD-type install assumed
INSTALL=install

# You don't have to change the definition variables in the folllowing.
#
CFLAGS = ${CCFLAGS} ${KPSEINC}
CNFCFLAGS = ${CFLAGS} -DDVI2PATH=\"${DVI2PATH}\" \
	    -DDVI2LIB=\"${DVI2LIB}\" -DFONTDESC=\"//${FONTDESC}\"

FILES = README doc Makefile lib-dist tools \
	defs.h xxstdio.h global.h commands.h set.h dconv.h \
	virfont.h bifont.h funcfont.h \
	jsub.h \
	dvi.h \
	dvi2.c dviconv.c dfdcom.c fontcom.c set.c rfset.c vfset.c \
	dconv.c run.c fontdesc.c scanfont.c \
	virfont.c vfdcom.c \
	bifont.c \
	compfont.c decompfont.c execfont.c \
	funcfont.c \
	kpse.c kanji.c open.c io.c util.c \
	xxstdio.c \
	version.c \
	dvi.c dvifont.c dvitfm.c

DVI2OBJS = dvi2.o dviconv.o dfdcom.o fontcom.o set.o rfset.o vfset.o \
	dconv.o run.o fontdesc.o scanfont.o \
	virfont.o vfdcom.o \
	bifont.o \
	compfont.o decompfont.o execfont.o \
	funcfont.o \
	kpse.o kanji.o open.o io.o util.o \
	xxstdio.o \
	version.o
DVIOBJS = dvi.o dvifont.o dvitfm.o

all: dvi2dvi

dvi2dvi: ${DVI2OBJS} ${DVIOBJS}
	${CC} -o $@ ${DVI2OBJS} ${DVIOBJS} ${KPSELIB}
dvi2.o: Makefile
	${CC} -c ${CNFCFLAGS} dvi2.c

newlib:
	-mkdir lib
	cp -pr lib-dist/* lib

install: install-dvi2dvi

install-dvi2dvi: dvi2dvi
	install -s -m 755 dvi2dvi ${BINAREA}/dvi2dvi

install-lib:
	-mkdir ${DVI2LIB}
	cp -pr lib/* ${DVI2LIB}
	chmod -R a+r ${DVI2LIB}

clean:	
	rm -f dvi2dvi *.o *.orig core

lint:
	lint *.c

dist:
	tar cf - ${FILES} | gzip > dvi2dvi.tar.gz

dvi2.o: dconv.h defs.h
dviconv.o: dconv.h defs.h set.h
dfdcom.o: dconv.h defs.h
fontcom.o: defs.h set.h
set.o: defs.h set.h
rfset.o: defs.h set.h
vfset.o: defs.h set.h
dconv.o : dconv.h
run.o: defs.h
fontdesc.o: defs.h
scanfont.o: dconv.h defs.h set.h
virfont.o: defs.h set.h virfont.h
vfdcom.o: dconv.h defs.h
bifont.o: defs.h bifont.h
compfont.o: defs.h set.h funcfont.h jsub.h
decompfont.o: defs.h set.h funcfont.h jsub.h
execfont.o: defs.h
funcfont.o: defs.h funcfont.h
kpse.o: defs.h
kanji.o: defs.h jsub.h
open.o: defs.h
io.o: defs.h
xxstdio.o: defs.h xxstdio.h

dvi.o: defs.h dvi.h
dvifont.o: defs.h dvi.h
dvitfm.o: defs.h bifont.h dvi.h
