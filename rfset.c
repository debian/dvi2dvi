/*
 * setchar for real font (non-virtual font)
 */

#include	"defs.h"
#include	"commands.h"
#include	"dconv.h"
#include	"global.h"
#include	"set.h"

void
realf_setchar(fe, c, Move)
struct font_entry *fe;
int c;
BOOLEAN Move;
{
    int cw;
    DEV_FONT devf;

    devf = fe->dev_fontdict(fe, c);

    if (devf != DEV_NULLFONT) {	/* ignore missing fonts */
	chmove = Move;
	dev_setfont(devf);
	dev_setposn(h, v);
	cw = fe->dev_setchar(fe, c);
	if (Move)
	    *move += cw;
    } else
	dev_setposn(h, v);

#ifdef STATS
    Stnc += 1;
    fe->ncts += 1;
#endif
}

#define	STRLEN		256
void
realf_setstring(fe, firstch)	/* read and set a consecutive string of chars */
struct font_entry *fe;
int firstch;
{
    char s[STRLEN];
    char *sp;
    int c, len;
    DEV_FONT devf;

    /* read entire string of chars */
    /* ensure that all characters are loaded, */
    devf = fe->dev_fontdict(fe, firstch);
    for (c = firstch, sp = s, len = 0;
	 c >= SETC_000 && c <= SETC_127 && len < STRLEN; len++) {
	if (devf != fe->dev_fontdict(fe, c))
	    break;
	*sp++ = c;
	c = DC_getcommand();
    }
    DC_backupone();

    /* NULL's are valid chars, so cant use for string termination */
    if (devf != DEV_NULLFONT) {	/* ignore missing fonts */
	chmove = TRUE;
	dev_setfont(devf);
	dev_setposn(h, v);
	*move += fe->dev_setstring(fe, s, len);
    } else
	dev_setposn(h, v);

#ifdef STATS
    Stnc += len;
    fe->ncts += len;
#endif
}
