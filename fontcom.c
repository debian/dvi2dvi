#include	"defs.h"
#include	"commands.h"
#include	"global.h"
#include	"set.h"

struct font_entry *hdfontent = NULL;
struct font_entry *curfontent;

/*-->ReadFontDef*/
/**********************************************************************/
/****************************  ReadFontDef  ***************************/
/**********************************************************************/

void
readfontdef(k, c, s, d, a, l, n, hdfip)
int k, c, s, d, a, l;
char *n;
struct font_index **hdfip;
{
    struct font_index *fi;

    fi = NEW(struct font_index, "font_index");
    fi->k = k;
    fi->next = *hdfip;
    *hdfip = fi;
#ifdef DEBUG
    if (Debug)
	(void)fprintf(stderr, "readfontdef %s k = %d s = %d d = %d",
		      n, k, s, d);
#endif
    fi->fent = get_font_entry(c, s, d, a, l, n);
}

struct font_entry *
get_font_entry(c, s, d, a, l, n)
int c, s, d, a, l;
char *n;
{
    struct font_entry *fe;
    char rn[STRSIZE];
    int rd, rs;
    int first_markchar();
    void read_null_fontinfo();

    if (replfont(n, s, rn, &rd, &rs)) {
#ifdef DEBUG
	if (Debug)
	    (void)fprintf(stderr, " -> %s s = %d d = %d", rn, rs, rd);
#endif
	n = rn;
	l = strlen(rn);
	d = rd;
	s = rs;
    }
    for (fe = hdfontent; fe != NULL; fe = fe->next)
	if (strcmp(n, fe->n) == 0 && s == fe->s) {
#ifdef DEBUG
	    if (Debug)
		(void)fprintf(stderr, "  [already read]\n");
#endif
	    return fe;
	}
#ifdef DEBUG
    if (Debug)
	(void)fprintf(stderr, "\n");
#endif

    fe = NEW(struct font_entry, "font_entry");
    fe->next = hdfontent;
    hdfontent = fe;
    fe->c = c;
    fe->s = s;
    fe->d = d;
    fe->a = a;
    fe->l = l;
    (void)strcpy(fe->n, n);
    fe->fnt_type = FT_UNDEF;
    fe->fnt_use = FU_NOTUSED;
    fe->vert_use = VU_UNDEF;
    fe->base_fe = fe;
    fe->openfile = NO_FILE;
    fe->ncdl = -1;
#ifdef STATS
    fe->nbpxl = 0;
    fe->ncts = 0;
#endif

    fe->fnt_markchar = first_markchar;
    fe->fnt_readfontinfo = read_null_fontinfo;

    return fe;
}

/*
 * We postpone the initialization of font info till the char in the font
 * is first used. This is because if the font is declared in the virtual
 * font, the font may be used only in some chars of vf and the chars may
 * not be used in a particular dvi.
 */
first_markchar(fe, c)
struct font_entry *fe;
int c;
{
    init_fontinfo(fe);
    fe->fnt_markchar(fe, c);
}


/*-->SetFntNum*/
/**********************************************************************/
/****************************  SetFntNum  *****************************/
/**********************************************************************/

void
SetFntNum(k, hdfidx)
int k;
struct font_index *hdfidx;
/*  this routine is used to specify the font to be used in printing future
    characters */
{
    struct font_index *ptr;

    for (ptr = hdfidx; (ptr!=NULL) && (ptr->k!=k); )
	ptr = ptr->next;
    if (ptr == NULL)
	Fatal("font %d undefined", k);
    setcurfont(ptr->fent);
}

/*
 * read_fontinfo
 */
void
read_fontinfo(fe)
struct font_entry *fe;
{
#ifdef DEBUG
    if (Debug)
	(void)fprintf(stderr, "<%s>\n", fe->n);
#endif
    /* most fonts are real fonts */
    fe->rvf_setchar = realf_setchar;
    fe->rvf_setstring = realf_setstring;
    (*(fe->fnt_readfontinfo))(fe);
}

/* operations for null font */

/* ARGSUSED */
null_markchar(fe, c)
struct font_entry *fe;
int c;
{
}

void
read_null_fontinfo(fe)
struct font_entry *fe;
{
    DEV_FONT null_fontdict();
    int null_setchar(), null_setstring();

    fe->dev_fontdict = null_fontdict;
    fe->dev_setchar = null_setchar;
    fe->dev_setstring = null_setstring;
}

/* ARGSUSED */
DEV_FONT
null_fontdict(fe, c)
struct font_entry *fe;
int c;
{
    return DEV_NULLFONT;
}

/* ARGSUSED */
null_setchar(c)
int c;
{
    return 0;
}

/* ARGSUSED */
null_setstring(s, len)
byte *s;
int len;
{
    return 0;
}
