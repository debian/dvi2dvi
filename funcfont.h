/*
 * functional font
 */

struct funcfont {
    struct fontop *ff_fop;
    BOOLEAN ff_body;
    struct ffinfo *ff_info;
    struct funcfont *ff_next;
};


/* decomp
 */

#define	CMP_NTT	0
#define	CMP_DCD	1

struct decomp {
    char dcmp_type;
    char *dcmp_name;
    struct font_entry *dcmp_fetab[1];
};

#define	dcmpfinfo(fe)	((struct decomp *)(fe->finfo))


/* comp
 */

struct comp {
    char cmp_type;
    char *cmp_name;
    struct font_entry *cmp_fe;
    unsigned short cmp_sub;
};

#define	cmpfinfo(fe)	((struct comp *)(fe->finfo))


/* repl
 */

struct repl {
    char *repl_replfont;
    char *repl_font;
    int repl_ds;
    int repl_fix;
};

#define	replfinfo(fe)	((struct repl *)(fe->finfo))
