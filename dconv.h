struct dconv {
    int (*dc_getcommand)();
    void (*dc_backupone)();
    void (*dc_getbytes)();
    void (*dc_skipbytes)();
    int (*dc_getuint)();
    int (*dc_getint)();
    FILE *dc_file;
    byte *dc_bufbeg;
    byte *dc_bufend;
    void (*dc_movedown)();
    void (*dc_movedown_v)();
    void (*dc_moveover)();
    void (*dc_moveover_v)();
    void (*dc_setrule)();
    void (*dc_setrule_v)();
    int dc_scale;
};

#define	DC_getcommand	(*dc_getcommand)
#define	DC_backupone	(*dc_backupone)
#define	DC_getbytes	(*dc_getbytes)
#define	DC_skipbytes	(*dc_skipbytes)
#define	DC_getuint	(*dc_getuint)
#define	DC_getint	(*dc_getint)
#define	DC_movedown	(*dc_movedown)
#define	DC_moveover	(*dc_moveover)
#define	DC_setrule	(*dc_setrule)
