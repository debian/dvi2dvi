#define	MAXVFCHAR	255	/* <= MAXMARKCHAR */

#define	VFD_UNDEF	0
#define	VFD_NOTLD	1
#define	VFD_NULL	2
#define	VFD_SETFC	3
#define	VFD_DVI		4

struct vfchar_entry {
    char vfdstat;
    union {
	struct {
	    struct font_entry *f;
	    int c;
	} set;
	struct {
	    unsigned int dvilen;
	    union {
		unsigned int fileoffset;
		unsigned char *dviptr;
	    } where;
	} dvi;
    } vfd;
    int tfmw;			/* TFM width */
};

struct virfntinfo {
    struct font_index *vf_fontidx;
    struct font_entry *vf_default_fent;
    struct vfchar_entry ch[1];
};

#ifdef ANSI
#define	virfinfo(fe)	(*(struct virfntinfo **)&(fe->finfo))
#else
#define	virfinfo(fe)	((struct virfntinfo *)(fe->finfo))
#endif

/* vf codes */
#define	VF_ID		202

#define	VF_SHORT_CHAR241	241
#define	VF_LONG_CHAR	242
#define	VF_FNT_DEF1	243
#define	VF_FNT_DEF2	244
#define	VF_FNT_DEF3	245
#define	VF_FNT_DEF4	246
#define	VF_PRE		247
#define	VF_POST		248

#include	"commands.h"
