/**********************************************************************/
/************************  Global Definitions  ************************/
/**********************************************************************/

#include	<stdio.h>
#include	<ctype.h>
#ifdef SYSVIO
#include	"xxstdio.h"
#endif

/* R_OK, F_OK */
#ifndef POSIX
#ifdef SYSV
#include	<sys/types.h>
#else
#include	<sys/file.h>
#endif
#else /* POSIX */
#include	<unistd.h>
#endif
/* Hack */
#ifndef R_OK
#define	R_OK	4
#endif
#ifndef F_OK
#define	F_OK	0
#endif
#ifndef SEEK_SET
#define	SEEK_SET	0
#define	SEEK_CUR	1
#define	SEEK_END	2
#endif

/* string operation */
#ifdef SYSV
#include	<string.h>
#define	index	strchr
#define	rindex	strrchr
#define	bcopy(s, d, n)	memcpy(d, s, n)
#define	bzero(p, n)	memset(p, 0, n)
#else
#include	<strings.h>
#endif

#ifndef POSIX
char *malloc();
char *realloc();
char *calloc();
int free();
#ifdef ANSI
char *sprintf(char *, const char *, ...);
#else
char *sprintf();
#endif
#else /* POSIX */
void *malloc();
void *realloc();
void *calloc();
void free();
#ifdef ANSI
int sprintf(char *, const char *, ...);
#else
int sprintf();
#endif
#endif
char *mktemp();
char *getenv();
void exit();

#define DEBUG		/* for massive printing of input */
			/* trace information; select by -d */
#define STATS		/* to enable statistics reporting via -s option */

#define	DEBUGoff	0
#define	DEBUGuser	1
#define	DEBUGsys	2
#define	Debugoff	(debug<=DEBUGoff)
#define	Debuguser	(debug>=DEBUGuser)
#define	Debug		(debug>=DEBUGsys)

#if defined(MSDOS) || defined(WIN32)
#define BINARYOPEN(f)	fopen(f,"rb")	/* byte-oriented host version */
#define BINARYWOPEN(f)	fopen(f,"w+b")
#define BINARYPOPEN(f)	popen(f,"rb")
/*#define	DIR_SEP		'\\'*/
#define	DIR_SEP		'/'
#define	PATH_SEP	';'
#define	TMP		"c:/temp"
#define	DVITEMPLATE	"D2DXXXXX"
#else
#define BINARYOPEN(f)	fopen(f,"r")	/* byte-oriented host version */
#define BINARYWOPEN(f)	fopen(f,"w+")
#define BINARYPOPEN(f)	popen(f,"r")
#define	DIR_SEP		'/'
#define	PATH_SEP	':'
#define	TMP		"/tmp/"
#define	DVITEMPLATE	"DVI2DVI-XXXXXX"
#endif

#ifdef WIN32
#define	STDC_HEADERS
#include	<io.h>
#include	<fcntl.h>
#define	O_BINARY	_O_BINARY
#define	SET_BINARY(fd)	_setmode((fd), _O_BINARY)
#endif

/* ARITHRSHIFT not used */
#define ARITHRSHIFT			/* define if ">>" operator is a */
					/* sign-propagating arithmetic  */
					/*   right shift */
#define USEGLOBALMAG			/* when defined, the dvi global */
					/*   magnification is applied   */
      
/* We can leave USEGLOBALMAG undefined when we have a limited
   number of font magnifications (at 300dpi) available.  Otherwise, we
   will simply complain about missing PXL files
 */
/* #undef USEGLOBALMAG */

#define	INCEXT		NULL

#define	MAXOPEN		12	/* limit on number of open font files */

#define	DVIFORMAT	2
#define	EXTDVIFORMAT	3

#define	STACKSIZE	100
#define	STRSIZE		257
#define	PATHLEN		1024
#define	NO_FILE		(FILE *)-1

#define	NEW(A,e)	((A *)alloc_check(malloc((unsigned)sizeof(A)), e))
#define	NEWTAB(A,n,e)	((A *)alloc_check(malloc((unsigned)sizeof(A)*n), e))
#define	MAGSIZE(f)	((unsigned int)(1000.0*(f) + 0.5))
#define	ROUND(f)	((int)((f) + 0.5))
#define	STREQ(s,t)	(strcmp(s,t) == 0)

typedef unsigned char byte;

/*
typedef float SCALED;
#define	scale(x,s)	(int)((SCALED)(x)*(SCALED)(s)/(1<<20))
*/
typedef int SCALED;
#define	scale(x,s)	scale_exact(x,s)

typedef int BOOLEAN;
typedef char Boolean;
#define	TRUE		1
#define	FALSE		0


/**********************************************************************/
/***********************  Font Data Structures  ***********************/
/**********************************************************************/

struct font_index {	/* pointer to font entry */
    int k;
    struct font_entry *fent;
    struct font_index *next;
};

typedef struct font_entry *DEV_FONT;
#define	DEV_NULLFONT	(DEV_FONT)NULL

#define	FT_UNDEF	0
#define	FT_DVI		1
#define	FT_TFM		2
#define	FT_JFM		3
#define	FT_VF		4
#define	FU_NOTUSED	0
#define	FU_USED		1

struct font_entry {	/* font entry */
    int k;			/* unique id	*/
    int c, s, d, a, l;
    char n[STRSIZE];		/* FNT_DEF command parameters		*/
    char fnt_type;
    char fnt_use;
    int vert_use;
    struct font_entry *vert_fe;
    struct font_entry *base_fe;
    void (*fnt_readfontinfo)();
    void (*rvf_setchar)();
    void (*rvf_setstring)();
    DEV_FONT (*dev_fontdict)();	/* returns the font in device		*/
    int (*dev_setchar)();	/* device dependent part of setchar	*/
    int (*dev_setstring)();	/* device dependent part of setstring	*/
    struct finfo *finfo;	/* font/device dependent information	*/
    struct font_entry *next;
    char name[PATHLEN];		/* full name of font file		*/
    FILE *openfile;		/* file stream (NO_FILE if none)	*/
    struct openfont_list *openlist;
    int ncdl;			/* # of different chars actually downloaded */
#ifdef STATS
    int nbpxl;			/* # of bytes of PXL data downloaded	*/
    int ncts;			/* total # of characters typeset	*/
#endif
};

#define	setcurfont(fe)	curfontent = fe
#define	fnt_markchar	dev_setchar
#define	FNT_markchar	DEV_setchar
#define	DEV_fontdict	(*(curfontent->dev_fontdict))
#define	DEV_setchar	(*(curfontent->dev_setchar))
#define	DEV_setstring	(*(curfontent->dev_setstring))

#define	VU_UNDEF	0
#define	VU_USED		1
#define	VU_NOTUSED	2
#define	VU_VERT		3

#define	NDIR	4
#define	HOR	0
#define	VER	1


struct fontop {
    char *fo_type;
    int (*fo_init)();
    int (*fo_access)();
    void (*fo_initfontinfo)();
};

#define	FDQUO	'"'
struct confop {
    char *co_name;
    void (*co_get)();
};

#define	ACC_EXACT	0
#define	ACC_SUBST	1
#define	ACC_MAGSTEP	2
#define	ACC_GEN		4
#define	ACC_SELF	8
#define	ACC_EXIST	(ACC_SUBST|ACC_MAGSTEP)
struct accarg {
    int acc_mode;
    float rawmagfact;
    float actmagfact;
    int submag, reqmag;
    float stepmagfact;
    char *pv_name;	/* font name */
    int pv_mag;		/* magnification */
    char *pv_fam;	/* family */
    int pv_ds;		/* design size */
    char *pv_jsub;	/* jsubfont name */
};

#define	DIR_KEEP	0
#define	DIR_ELIM	1
#define	DIR_SPECIAL	2
#define	DIR_IGNORE	3
#define	DIR_MARKLINE	4
#define	DIR_MARKCHAR	5

#define	dir_with_width	(dirmode >= DIR_SPECIAL)
