#ifdef KPATHSEA

#include	"defs.h"
#include	<kpathsea/pathsearch.h>
#include	<kpathsea/tex-file.h>
#include	<kpathsea/tex-glyph.h>

char *
kpsearch_glyph(proto, n, format, acca, name)
char *proto, *n;
kpse_file_format_type format;
struct accarg *acca;
char *name;
{
    char *path, *base;
    const_string save_path, save_orpath;
    kpse_format_info_type *spec = &kpse_format_info[format];
    kpse_glyph_file_type font_file;
    char *filename;

    pavek(name, &path, &base, proto, acca);
    if (path != NULL) {
	save_path = spec->path;
	save_orpath = spec->override_path;
	spec->path = NULL;
	spec->override_path = path;
    }
    if (spec->path == NULL)
	kpse_init_format(format);
    if (acca->acc_mode&ACC_GEN)
	spec->program_enabled_p = true;
    if (base == NULL)
	filename = kpse_find_glyph(n, acca->pv_mag, format, &font_file);
    else if (path == NULL)
	filename = kpse_find_glyph(base, acca->pv_mag, format, &font_file);
    else if (path == NULL)
	filename = kpse_find_file(base, format, true);
    else
	filename = kpse_path_search(spec->path, base, false);
    if (path != NULL) {
	spec->path = save_path;
	spec->override_path = save_orpath;
    }
    if (acca->acc_mode&ACC_GEN)
	spec->program_enabled_p = false;
    return filename;
}

char *
kpsearch_make(proto, n, format, acca, name)
char *proto, *n;
kpse_file_format_type format;
struct accarg *acca;
char *name;
{
    char *path, *base;
    const_string save_path, save_orpath;
    kpse_format_info_type *spec = &kpse_format_info[format];
    char *filename;

    pavek(name, &path, &base, proto, acca);
    if (path != NULL) {
	save_path = spec->path;
	save_orpath = spec->override_path;
	spec->path = NULL;
	spec->override_path = path;
    }
    if (spec->path == NULL)
	kpse_init_format(format);
    if (acca->acc_mode&ACC_GEN)
	spec->program_enabled_p = true;
    if (base == NULL)
	filename = kpse_find_file(n, format, true);
    else if (path == NULL)
	filename = kpse_find_file(base, format, true);
    else
	filename = kpse_path_search(spec->path, base, false);
    if (path != NULL) {
	spec->path = save_path;
	spec->override_path = save_orpath;
    }
    if (acca->acc_mode&ACC_GEN)
	spec->program_enabled_p = false;
    return filename;
}

char *
kpsearch_file(proto, n, format)
char *proto, *n;
kpse_file_format_type format;
{
    char pathname[PATHLEN];	/* dummy */
    char *path, *base;
    const_string save_path, save_orpath;
    kpse_format_info_type *spec = &kpse_format_info[format];
    char *filename;

    pavek(pathname, &path, &base, proto, NULL);
    if (path != NULL) {
	save_path = spec->path;
	save_orpath = spec->override_path;
	spec->path = NULL;
	spec->override_path = path;
    }
    if (spec->path == NULL)
	kpse_init_format(format);
    if (base == NULL)
	filename = kpse_find_file(n, format, true);
    else if (path == NULL)
	filename = kpse_find_file(base, format, true);
    else
	filename = kpse_path_search(spec->path, base, false);
    if (path != NULL) {
	spec->path = save_path;
	spec->override_path = save_orpath;
    }
    return filename;
}

#endif
