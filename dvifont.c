#include	"defs.h"
#include	"commands.h"
#include	"global.h"
#include	"dvi.h"

void
init_dvi_fontinfo(fe)
struct font_entry *fe;
{
    int	null_markchar();
    void read_dvi_fontinfo();

    fe->fnt_type = FT_DVI;
    /*fe->base_fe->fnt_use = FU_USED;*/
    fe->fnt_markchar = null_markchar;
    fe->fnt_readfontinfo = read_dvi_fontinfo;
}

void
read_dvi_fontinfo(fe)
struct font_entry *fe;
{
    DEV_FONT dvi_fontdict();
    int dvi_setchar(), dvi_setstring();

    fe->dev_fontdict = dvi_fontdict;
    fe->dev_setchar = dvi_setchar;
    fe->dev_setstring = dvi_setstring;
}

/* ARGSUSED */
DEV_FONT
dvi_fontdict(fe, c)
struct font_entry *fe;
int c;
{
    return fe->base_fe;
}

dvichar(c)
unsigned int c;
{
    byte b[sizeof(int)];
    int n;

    if (chmove) {
	if (c <= SETC_127) {
	    putbyte(outfp, c);
	    dvipos++;
	} else {
	    n = uinttob(b, c);
	    putbyte(outfp, SET1+n-1);
	    putbytes(outfp, b, n);
	    dvipos += n+1;
	}
    } else {
	n = uinttob(b, c);
	putbyte(outfp, PUT1+n-1);
	putbytes(outfp, b, n);
	dvipos += n+1;
    }
}

dvi_setchar(fe, c)
struct font_entry *fe;
unsigned int c;
{
    begin_string();
    dvichar(c);
}

dvi_setstring(fe, s, len)
struct font_entry *fe;
byte *s;
int len;
{
    int i;

    begin_string();
    for (i = 0; i < len; i++, s++)
	putbyte(outfp, *s);
    dvipos += len;
}
