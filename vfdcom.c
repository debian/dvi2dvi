/*
 * funtions for analyzing dvi in vf-file (vfd stands for vf-file dvi)
 */

#include	"defs.h"
#include	"commands.h"
#include	"dconv.h"
#include	"global.h"

int vfd_getcommand();
void vfd_backupone();
void vfd_getbytes();
void vfd_skipbytes();
int vfd_getuint();
int vfd_getint();
void vfd_movedown();
void vfd_movedown_v();
void vfd_moveover();
void vfd_moveover_v();
void vfd_setrule();
void vfd_setrule_v();
struct dconv vfd_dconv_templ = {
    vfd_getcommand,
    vfd_backupone,
    vfd_getbytes,
    vfd_skipbytes,
    vfd_getuint,
    vfd_getint,
    NULL,
    NULL,
    NULL,
    vfd_movedown,
    vfd_movedown_v,
    vfd_moveover,
    vfd_moveover_v,
    vfd_setrule,
    vfd_setrule_v,
    0
};

vfd_dirkeep()
{
    vfd_dconv_templ.dc_movedown_v = vfd_dconv_templ.dc_movedown;
    vfd_dconv_templ.dc_moveover_v = vfd_dconv_templ.dc_moveover;
    vfd_dconv_templ.dc_setrule_v = vfd_dconv_templ.dc_setrule;
}

int
vfd_getcommand()
{
    dc_bufbeg++;
    return (dc_bufbeg > dc_bufend ? POST : *(dc_bufbeg-1));
}

void
vfd_backupone()
{
    --dc_bufbeg;
}

void
vfd_getbytes(cp, n)
char *cp;
int n;
{
    (void)strncpy(cp, (char *)dc_bufbeg, n);
    dc_bufbeg += n;
}

void
vfd_skipbytes(n)
int n;
{
    dc_bufbeg += n;
}

int
vfd_getuint(n)
int n;
{
    int x;

    x = makeuint(dc_bufbeg, n);
    dc_bufbeg += n;
    return x;
}

int
vfd_getint(n)
int n;
{
    int x;

    x = makeint(dc_bufbeg, n);
    dc_bufbeg += n;
    return x;
}


extern int command;

void
vfd_movedown(a, n)
byte *a;
int n;
{
    if (n == 0)
	dev_move0(command);
    else
	dev_makemove(scale(makeint(a, n), dc_scale), command-n);
}

void
vfd_movedown_v(a, n, l)
byte *a;
int n, l;
{
    if (DOWN1 <= command && command <= DOWN4)
	l = makeint(a, n);
    dev_makemove(-scale(l, dc_scale), RIGHT1-1);
}

void
vfd_moveover(b, n)
byte *b;
int n;
{
    if (n == 0)
	dev_move0(command);
    else
	dev_makemove(scale(makeint(b, n), dc_scale), command-n);
}

void
vfd_moveover_v(b, n, l)
byte *b;
int n, l;
{
    if (RIGHT1 <= command && command <= RIGHT4)
	l = makeint(b, n);
    dev_makemove(scale(l, dc_scale), DOWN1-1);
}

void
vfd_setrule(a, b, Set)	/* this routine will draw a rule */
byte *a, *b;
BOOLEAN Set;
{
    dev_setrule(scale(makeint(a, 4), dc_scale),
		scale(makeint(b, 4), dc_scale), command);
}

void
vfd_setrule_v(a, b, Set)	/* this routine will draw a rule */
byte *a, *b;
BOOLEAN Set;
{
    d_setrule_v(scale(makeint(a, 4), dc_scale),
		scale(makeint(b, 4), dc_scale), Set);
}
