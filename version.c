/*
 * version 0.1  (Oct 1992)
 * version 0.2  (Dec 1992)
 * version 0.3  (Dec 1992)
 * version 0.4  (Jun 1993)
 * version 0.5  (Sep 1993)
 * version 0.6  (Oct 1993)
 * version 0.7  (Nov 1993)
 * version 0.8  (Jan 1994)
 * version 0.9  (Nov 1994)
 * version 1.0 alpha  (Mar 1995)
 * version 1.0 alpha1 (Oct 1995)
 * version 1.0  (May 1996)
 * version 1.01 (May 1998)
 * version 1.1  (Sep 1999)
 * version 1.2  (May 2000)
 * version 2.0 alpha  (May 2000)
 */

char *version = "@(#)dvi2dvi 2.0 alpha";
