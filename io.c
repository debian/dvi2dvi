#include	"defs.h"
#include	"global.h"

/*-->getbytes*/
/**********************************************************************/
/*****************************  getbytes  *****************************/
/**********************************************************************/

void
getbytes(fp, cp, n)	/* get n bytes from file fp */
FILE *fp;	/* file pointer	 */
byte *cp;	/* character pointer */
int n;		/* number of bytes  */
{
    (void)fread((char *)cp, 1, n, fp);
}

void
skipbytes(fp, n)
FILE *fp;
int n;
{
    (void)fseek(fp, (long)n, SEEK_CUR);
}


/*-->getuint*/
/**********************************************************************/
/***************************  getuint  ********************************/
/**********************************************************************/

int
getuint(fp, n)	/* return n byte quantity from file fd */
FILE *fp;	/* file pointer    */
int n;		/* number of bytes */
{
    byte s[sizeof(int)];

    (void)fread((char *)s, 1, n, fp);
    return makeuint(s, n);
}


/*-->getint*/
/**********************************************************************/
/****************************  getint  ********************************/
/**********************************************************************/

int
getint(fp, n)	/* return n byte quantity from file fd */
FILE *fp;	/* file pointer    */
int n;		/* number of bytes */
{
    byte s[sizeof(int)];

    (void)fread((char *)s, 1, n, fp);
    return makeint(s, n);
}


/**********************************************************************/
/**********************************************************************/

putbyte(fp, c)
FILE *fp;
byte c;
{
    (void)putc(c, fp);
}

copybytes(sfp, dfp, n)
FILE *sfp, *dfp;	/* file pointer    */
int n;		/* number of bytes */
{
    for (; n > 0; --n)
	(void)putc(getc(sfp), dfp);
}

putbytes(fp, cp, n)
FILE *fp;
byte *cp;
int n;
{
    (void)fwrite((char *)cp, 1, n, fp);
}

putnint(fp, x, n)
FILE *fp;	/* file pointer	 */
unsigned int x;
int n;		/* number of bytes */
{
    if (n > 0) {
	putnint(fp, x>>8, n-1);
	(void)putc((int)x&0xff, fp);
    }
}
