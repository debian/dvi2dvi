#include	"defs.h"
#include	"global.h"
#include	"commands.h"
#include	"bifont.h"
#include	"dvi.h"

extern int dirmode;

/* ARGSUSED */
dev_tfm_initfontdict(fe)
struct font_entry *fe;
{
}

dev_tfm_initfe(fe)
struct font_entry *fe;
{
    DEV_FONT dvi_fontdict();
    int tfm_setchar(), tfm_setstring();
    int blank_setchar(), blank_setstring();
    int mchar_setchar(), mchar_setstring();
    int dvi_setchar(), dvi_setstring();

    fe->dev_fontdict = dvi_fontdict;
    if (fe->vert_use == VU_VERT) {
	if (dirmode == DIR_KEEP || dirmode == DIR_ELIM) {
	    fe->dev_setchar = dvi_setchar;
	    fe->dev_setstring = dvi_setstring;
	} else if (dirmode == DIR_SPECIAL) {
	    fe->dev_setchar = tfm_setchar;
	    fe->dev_setstring = tfm_setstring;
	} else if (dirmode == DIR_IGNORE || dirmode == DIR_MARKLINE) {
	    fe->dev_setchar = blank_setchar;
	    fe->dev_setstring = blank_setstring;
	} else if (dirmode == DIR_MARKCHAR) {
	    fe->dev_setchar = mchar_setchar;
	    fe->dev_setstring = mchar_setstring;
	}
    } else {
	fe->dev_setchar = dvi_setchar;
	fe->dev_setstring = dvi_setstring;
    }
}

tfm_setchar(fe, c)
struct font_entry *fe;
int c;
{
    struct tfmchar_entry *ce = &(tfmfinfo(fe)->ch[c]);
    int cw;

    begin_string();
    dvichar(ce->dev_char);
    *dvi_move += (cw = ce->tfmw);
    return cw;
}

tfm_setstring(fe, s, len)
struct font_entry *fe;
char *s;
int len;
{
    char *sp;
    struct tfmchar_entry *ce = tfmfinfo(fe)->ch;
    int cw;

    begin_string();
    for (sp = s, cw = 0; sp < s+len; sp++) {
	putbyte(outfp, (ce+*sp)->dev_char);
	cw += (ce+*sp)->tfmw;
    }
    *dvi_move += cw;
    dvipos += len;
    return cw;
}

blank_setchar(fe, c)
struct font_entry *fe;
int c;
{
    struct tfmchar_entry *ce = &(tfmfinfo(fe)->ch[c]);
    int cw;

    begin_string();
    *dvi_move += (cw = ce->tfmw);
    return cw;
}

blank_setstring(fe, s, len)
struct font_entry *fe;
char *s;
int len;
{
    char *sp;
    struct tfmchar_entry *ce = tfmfinfo(fe)->ch;
    int cw;

    begin_string();
    for (sp = s, cw = 0; sp < s+len; sp++) {
	cw += (ce+*sp)->tfmw;
    }
    *dvi_move += cw;
    return cw;
}

mchar_setchar(fe, c)
struct font_entry *fe;
int c;
{
    struct tfmchar_entry *ce = &(tfmfinfo(fe)->ch[c]);
    int cw;

    dev_makemove(ce->tfmw, DOWN1-1);
    dvi_push();
    dvichar(ce->dev_char);
    dvi_pop();
    *dvi_move += (cw = ce->tfmw);
    return cw;
}

mchar_setstring(fe, s, len)
struct font_entry *fe;
char *s;
int len;
{
    char *sp;
    struct tfmchar_entry *ce = tfmfinfo(fe)->ch;
    int cw;

    begin_string();
    for (sp = s, cw = 0; sp < s+len; sp++) {
	dev_makemove((ce+*sp)->tfmw, DOWN1-1);
	dvi_push();
	putbyte(outfp, (ce+*sp)->dev_char);
	dvi_pop();
	cw += (ce+*sp)->tfmw;
    }
    *dvi_move += cw;
    dvipos += len;
    return cw;
}

dev_vftfm_char(fe, c, tfmw)
struct font_entry *fe;
int c, tfmw;
{
    dev_setfont(fe);
    dvi_vfchar(c, tfmw-tfmfinfo(fe)->ch[c].tfmw);
}

/* ARGSUSED */
dev_jfm_initfontdict(fe)
struct font_entry *fe;
{
}

dev_jfm_initfe(fe, id)
struct font_entry *fe;
int id;
{
    DEV_FONT dvi_fontdict();
    int dvi_setchar(), dvi_setstring();

    fe->dev_fontdict = dvi_fontdict;
    fe->dev_setchar = dvi_setchar;
    fe->dev_setstring = dvi_setstring;
}

dev_vfjfm_char(fe, c, tfmw)
struct font_entry *fe;
int c, tfmw;
{
    dev_setfont(fe);
    dvi_vfchar(c, tfmw-jfmfinfo(fe)->ch[getctype(c,jfmfinfo(fe))].tfmw);
}

dvi_vfchar(c, dtfmw)
int c, dtfmw;
{
    begin_string();
    dvichar(c);
    if (chmove && dtfmw != 0)
	dev_makemoveover(dtfmw);
}
