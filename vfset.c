/*
 * setchar for virtual font
 */

#include	"defs.h"
#include	"commands.h"
#include	"dconv.h"
#include	"global.h"
#include	"set.h"

void
virf_setchar(fe, c, Move)
struct font_entry *fe;
int c;
BOOLEAN Move;
{
    int cw;

    chmove = Move;
    cw = fe->dev_setchar(fe, c);
    if (Move)
	*move += cw;

#ifdef STATS
    Stnc += 1;
    fe->ncts += 1;
#endif
}

#define	STRLEN		256
void
virf_setstring(fe, firstch)	/* read and set a consecutive string of chars */
struct font_entry *fe;
int firstch;
{
    char s[STRLEN];
    char *sp;
    int c, len;

    /* read entire string of chars */
    /* ensure that all characters are loaded, */
    for (c = firstch, sp = s, len = 0;
	 c >= SETC_000 && c <= SETC_127 && len < STRLEN; len++) {
	*sp++ = c;
	c = DC_getcommand();
    }
    DC_backupone();

    chmove = TRUE;
    *move += fe->dev_setstring(fe, s, len);

#ifdef STATS
    Stnc += len;
    fe->ncts += len;
#endif
}
