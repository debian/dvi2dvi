#include	"defs.h"
#include	"commands.h"
#include	"dconv.h"
#include	"global.h"
#include	"set.h"

extern int dirmode;

struct font_entry *
getcurrentfontent()
{
    static struct font_entry *new_vfe = NULL;

    if (dir == VER) {
	if (curfontent->vert_use == VU_UNDEF) {
	    if (new_vfe == NULL)
		new_vfe = NEW(struct font_entry, "font_entry");
	    *new_vfe = *curfontent;
	    new_vfe->openfile = NO_FILE;
	    if (init_vfontinfo(new_vfe)) {
		curfontent->vert_use = VU_USED;
		curfontent->vert_fe = new_vfe;
		curfontent->vert_fe->base_fe = curfontent;
		curfontent->vert_fe->vert_use = VU_VERT;
		new_vfe = NULL;
	    } else
		curfontent->vert_use = VU_NOTUSED;
	    if (dir_with_width) {
		if (curfontent->vert_use == VU_NOTUSED) {
		    if (curfontent->fnt_type == FT_UNDEF)
			init_fontinfo(curfontent);
		    if (curfontent->fnt_type == FT_DVI)
			Fatal("font info necessary for vertical use of %s\n",
			      curfontent->n);
		}
		if (curfontent->vert_use == VU_NOTUSED) {
		    curfontent->vert_use = VU_USED;
		    curfontent->vert_fe = NEW(struct font_entry, "font_entry");
		    *(curfontent->vert_fe) = *curfontent;
		    curfontent->vert_fe->base_fe = curfontent;
		    curfontent->vert_fe->openfile = NO_FILE;
		}
		curfontent->vert_fe->vert_use = VU_VERT;
	    }
	}
	if (curfontent->vert_use == VU_USED)
	    return curfontent->vert_fe;
    }
    return curfontent;
}

struct font_entry *
currentfontent()
{
    if (dir == VER) {
	if (curfontent->vert_use == VU_USED)
	    return curfontent->vert_fe;
    }
    return curfontent;
}

void
MarkChar(c)
int c;
{
    struct font_entry *fe;

    fe = getcurrentfontent();
    fe->fnt_markchar(fe, c);
}

void
MarkString(firstch)
int firstch;
{
    int c;
    struct font_entry *fe;

    fe = getcurrentfontent();
    for (c = firstch; c >= SETC_000 && c <= SETC_127; ) {
	fe->fnt_markchar(fe, c);
	c = DC_getcommand();
    }
    DC_backupone();
}

void
SetChar(c, Move)
int c;
BOOLEAN Move;
{
    struct font_entry *fe;

    fe = currentfontent();
    fe->rvf_setchar(fe, c, Move);
}

void
SetString(firstch)
int firstch;
{
    struct font_entry *fe;

    fe = currentfontent();
    fe->rvf_setstring(fe, firstch);
}
