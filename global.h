/* dvi2.c */
extern int FirstPage, LastPage;

extern int mag, usermag;
extern struct font_entry *hdfontent, *curfontent;
extern char *dvi2path;
extern char *dvi2lib;
extern int resolution;

extern char dvifilename[];
extern char dvidirpath[];
extern char rootname[];
extern FILE *dvifp;
extern FILE *outfp;

extern int ncopies;
extern int ndone;

#ifdef DEBUG
extern int debug;
#endif
extern int PreLoad;
extern int Reverse;
extern int G_errenc;
extern char G_Logname[];
extern int G_logging;
extern int G_logfile;
extern FILE *G_logfp;
extern char *G_progname;
extern int G_quiet;
extern int G_nowarn;
extern int G_removecomments;
#ifdef MSDOS
extern int G_longfontname;
#endif
extern int SpecialSize;
extern char *SpecialStr;
#ifdef STATS
extern int Stats;
extern int Snbpxl;
extern int Sonbpx;
extern int Sndc;
extern int Stnc;
extern int Snbpx0, Sndc0, Stnc0;
#endif

/* dviconv.c */
extern int h, v;
extern int dir;
extern int *move;
extern BOOLEAN chmove;

/* dfdcom.c */
void ReadFontDef();
void SkipFontDef();

/* fontcom.c */
void readfontdef();
struct font_entry *get_font_entry();
void SetFntNum();
void read_fontinfo();

/* dconv.c */
extern struct dconv *curdconvp;
extern struct dconv dfd_dconv_templ, vfd_dconv_templ;
extern int (*dc_getcommand)();
extern void (*dc_backupone)();
extern void (*dc_getbytes)();
extern void (*dc_skipbytes)();
extern int (*dc_getuint)();
extern int (*dc_getint)();
extern FILE *dc_file;
extern byte *dc_bufbeg;
extern byte *dc_bufend;
extern void (*dc_movedown)();
extern void (*dc_moveover)();
extern void (*dc_setrule)();
extern int dc_scale;

/* run.c */
void init_settings();
void AbortRun();
void AllDone();
void Fatal();
void Warning();

/* mag.c */
float actfact();
float apprfact();
extern float mag_table[];
extern int mag_index;
extern int magtabsize;

/* fontdesc.c */
void read_fontdesc();
void skipline();
void skipblanks();
struct fontop *findfontop();
int pathtype_init();
void arg_fontdesc();
void defexpand();
void add_include();
void add_setup();
void init_fontinfo();
void pave();

/* open.c */
void openfontfile();

/* io.c */
void getbytes();
void skipbytes();
int getuint();
int getint();
void putint();
void putoct();

/* util.c */
char *alloc_check();
int makeuint();
int makeint();
char *strsave();

/* version.c */
extern char *version;

/*
 * Interface with device driver
 */
/* ps.c */
float dev_fontmag();

/* psspecial.c */
void dev_predospecial();
void dev_dospecial();

/* psio.c */
void dev_copyfile();
void dev_copystring();

#ifdef KPATHSEA
/*
 *
 */
/* kpse.c */
char *kpsearch_glyph();
char *kpsearch_make();
char *kpsearch_file();
#endif
