/*
 * exec font
 */

#include	"defs.h"
#include	"global.h"

int cexectype_access();
int exectype_access();
void init_exec_fontinfo();
struct fontop cexecop = {
    "exec",
    pathtype_init,
    cexectype_access,
    init_exec_fontinfo,	/* never called */
};
struct fontop execop = {
    "exec",
    pathtype_init,
    exectype_access,
    init_exec_fontinfo,	/* never called */
};

int
cexectype_access(proto, fe, acca)
char *proto;
struct font_entry *fe;
struct accarg *acca;
{
    FILE *f;
    int c;
    char execline[PATHLEN];
    int stat;

    if (acca->acc_mode != ACC_EXACT)
	return FALSE;

    acca->pv_mag = ROUND(acca->actmagfact*resolution);
    pave(execline, proto, acca);
#ifdef DEBUG
    if (Debuguser)
	(void)fprintf(stderr, "trying to exec for %s\n\t%s\n",
		      fe->n, execline);
#endif

    if ((f = BINARYPOPEN(execline)) != NULL) {
	if (Debug)
	    while ((c = getc(f)) != EOF)
		putc(c, stderr);
	else
	    while ((c = getc(f)) != EOF)
		;
	if ((stat = pclose(f)) != 0)
	    Warning("exec font generation failed : %s", execline);
    } else
	Warning("exec font execution failed : %s", execline);

    return !stat;
}

int
exectype_access(proto, fe, acca)
char *proto;
struct font_entry *fe;
struct accarg *acca;
{
    (void)cexectype_access(proto, fe, acca);
    return FALSE;
}

/* ARGSUSED */
void
init_exec_fontinfo(fe)
struct font_entry *fe;
{
    Fatal("%s implementation error: init_exec_fontinfo", G_progname);
}
