/*
 * funtions for analyzing dvi in dvi-file (dfd stands for dvi-file dvi)
 */

#include	"defs.h"
#include	"commands.h"
#include	"dconv.h"
#include	"global.h"

int dfd_getcommand();
void dfd_backupone();
void dfd_getbytes();
void dfd_skipbytes();
int dfd_getuint();
int dfd_getint();
void dfd_movedown();
void dfd_movedown_v();
void dfd_moveover();
void dfd_moveover_v();
void dfd_setrule();
void dfd_setrule_v();
struct dconv dfd_dconv_templ = {
    dfd_getcommand,
    dfd_backupone,
    dfd_getbytes,
    dfd_skipbytes,
    dfd_getuint,
    dfd_getint,
    NULL,
    NULL,
    NULL,
    dfd_movedown,
    dfd_movedown_v,
    dfd_moveover,
    dfd_moveover_v,
    dfd_setrule,
    dfd_setrule_v,
    0
};

dfd_dirkeep()
{
    dfd_dconv_templ.dc_movedown_v = dfd_dconv_templ.dc_movedown;
    dfd_dconv_templ.dc_moveover_v = dfd_dconv_templ.dc_moveover;
    dfd_dconv_templ.dc_setrule_v = dfd_dconv_templ.dc_setrule;
}

int
dfd_getcommand()
{
    return getuint(dc_file, 1);
}

void
dfd_backupone()
{
    (void)fseek(dc_file, -1L, SEEK_CUR);
}

void
dfd_getbytes(cp, n)
byte *cp;
int n;
{
    getbytes(dc_file, cp, n);
}

void
dfd_skipbytes(n)
int n;
{
    (void)fseek(dc_file, (long)n, SEEK_CUR);
}

int
dfd_getuint(n)
int n;
{
    return getuint(dc_file, n);
}

int
dfd_getint(n)
int n;
{
    return getint(dc_file, n);
}


void
ReadFontDef(k, hdfip)
int k;
struct font_index **hdfip;
{
    int	c, s, d, a, l;
    byte n[STRSIZE];

    c = getuint(dc_file, 4);	/* checksum */
    s = getuint(dc_file, 4);	/* scaled size */
    d = getuint(dc_file, 4);	/* design size */
    a = getuint(dc_file, 1);	/* area length for font name */
    l = getuint(dc_file, 1);	/* device length */
    getbytes(dc_file, n, a+l);
    n[a+l] = '\0';
    readfontdef(k, c, s, d, a, l, (char *)n, hdfip);
}

void
SkipFontDef()
{
    int a, l;

    skipbytes(dc_file, 4*3);
    a = getuint(dc_file, 1);
    l = getuint(dc_file, 1);
    skipbytes(dc_file, a+l);
}

int
HasBeenRead(k, hdfidx)
int k;
struct font_index *hdfidx;
{
    struct font_index *ptr;

    for (ptr = hdfidx; (ptr!=NULL) && (ptr->k!=k); )
	ptr = ptr->next;
    return (ptr != NULL);
}


extern int command;

void
dfd_movedown(a, n, l)
byte *a;
int n, l;
{
    dev_move(a, n, command-n);
}

void
dfd_movedown_v(a, n, l)
byte *a;
int n, l;
{
    if (DOWN1 <= command && command <= DOWN4)
	l = makeint(a, n);
    dev_makemove(-l, RIGHT1-1);
}

void
dfd_moveover(b, n, l)
byte *b;
int n, l;
{
    dev_move(b, n, command-n);
}

void
dfd_moveover_v(b, n, l)
byte *b;
int n, l;
{
    if (RIGHT1 <= command && command <= RIGHT4)
	l = makeint(b, n);
    dev_makemove(l, DOWN1-1);
}

void
dfd_setrule(a, b, Set)	/* this routine will draw a rule */
byte *a, *b;
BOOLEAN Set;
{
    dev_setrule(makeint(a, 4), makeint(b, 4), command);
}

void
d_setrule_v(x, y, Set)
int x, y;
BOOLEAN Set;
{
    if (x <= 0 || y <= 0) {
	if (Set)
	    dev_makemove(y, DOWN1-1);
    } else {
	if (!Set)
	    dev_push();
	dev_makemove(y, DOWN1-1);
	dev_setrule(y, x, PUT_RULE);
	if (!Set)
	    dev_pop();
    }
}

void
dfd_setrule_v(a, b, Set)	/* this routine will draw a rule */
byte *a, *b;
BOOLEAN Set;
{
    d_setrule_v(makeint(a, 4), makeint(b, 4), Set);
}
