#include	"defs.h"
#include	"dconv.h"
#include	"global.h"

struct dconv *curdconvp;
int (*dc_getcommand)();
void (*dc_backupone)();
void (*dc_getbytes)();
void (*dc_skipbytes)();
int (*dc_getuint)();
int (*dc_getint)();
FILE *dc_file;
byte *dc_bufbeg;
byte *dc_bufend;
void (*dc_movedown)();
void (*dc_moveover)();
void (*dc_setrule)();
int dc_scale;

setcurdconv(dp)
struct dconv *dp;
{
    curdconvp = dp;
    dc_getcommand = dp->dc_getcommand;
    dc_backupone = dp->dc_backupone;
    dc_getbytes = dp->dc_getbytes;
    dc_skipbytes = dp->dc_skipbytes;
    dc_getuint = dp->dc_getuint;
    dc_getint = dp->dc_getint;
    dc_file = dp->dc_file;
    dc_bufbeg = dp->dc_bufbeg;
    dc_bufend = dp->dc_bufend;
    if (dir == HOR) {
	dc_movedown = dp->dc_movedown;
	dc_moveover = dp->dc_moveover;
	dc_setrule = dp->dc_setrule;
    } else {
	dc_movedown = dp->dc_movedown_v;
	dc_moveover = dp->dc_moveover_v;
	dc_setrule = dp->dc_setrule_v;
    }
    dc_scale = dp->dc_scale;
}

setdirhor()
{
    dc_movedown = curdconvp->dc_movedown;
    dc_moveover = curdconvp->dc_moveover;
    dc_setrule = curdconvp->dc_setrule;
}

setdirver()
{
    dc_movedown = curdconvp->dc_movedown_v;
    dc_moveover = curdconvp->dc_moveover_v;
    dc_setrule = curdconvp->dc_setrule_v;
}
