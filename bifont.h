#define	LASTTFMCHAR	255
#define	NTFMCHARS	256

struct tfmchar_entry {		/* character entry */
    short dev_font, dev_char;
    int tfmw;			/* TFM width */
};

struct tfmfntinfo {
    struct bifont *tfm_bf;
    int lastfntchar;
    struct tfmchar_entry ch[NTFMCHARS];	/* character information */
};

#ifdef ANSI
#define	tfmfinfo(fe)	(*(struct tfmfntinfo **)&(fe->finfo))
#else
#define	tfmfinfo(fe)	((struct tfmfntinfo *)(fe->finfo))
#endif


struct jstfmchar_entry {	/* character entry */
    unsigned short dev_ku, dev_ten;
    int tfmw;			/* TFM width */
};

struct jstfmfntinfo {
    struct bifont *js_bf;	/* = js_share->jss_bf */
    int lastfntchar;
    struct jstfmchar_entry ch[LASTTFMCHAR+1];	/* character information */
    short jsubfont;
    short dev_font;		/* = js_share->jss_dev_font */
    struct jssinfo *js_info;	/* = js_share->jss_info */
    struct jsubshare *js_share;
};

#ifdef ANSI
#define	jstfmfinfo(fe)	(*(struct jstfmfntinfo **)&(fe->finfo))
#else
#define	jstfmfinfo(fe)	((struct jstfmfntinfo *)(fe->finfo))
#endif


#define	JFM_ID		11
#define	TJFM_ID		9

struct jfmtype_entry {		/* character type entry */
    int jfm_code;
    int jfm_type;
};

struct jfmchar_entry {		/* character entry */
    int tfmw;			/* TFM width */
};

struct jfmfntinfo {
    struct bifont *jfm_bf;
    int nctype;
    struct jfmtype_entry *ctype;	/* character type information */
    int lasttypecode;
    struct jfmchar_entry *ch;	/* character information indexed by type */
};

#ifdef ANSI
#define	jfmfinfo(fe)	(*(struct jfmfntinfo **)&(fe->finfo))
#else
#define	jfmfinfo(fe)	((struct jfmfntinfo *)(fe->finfo))
#endif


#ifdef UNDEF
struct wlchar_entry {		/* character entry */
    short dev_font, dev_char;
    int tfmw;			/* TFM width */
};

struct wlfntinfo {
    struct bifont *wl_bf;
    int nfntchars;
    struct wlchar_entry ch[1];	/* character information	*/
};

#ifdef ANSI
#define	wlfinfo(fe)	(*(struct wlfntinfo **)&(fe->finfo))
#else
#define	wlfinfo(fe)	((struct wlfntinfo *)(fe->finfo))
#endif

#ifdef ANSI
#define	jswlfinfo(jsfi)	(*(struct wlfntinfo **)&(jsfi->js_info))
#else
#define	jswlfinfo(jsfi)	((struct wlfntinfo *)(jsfi->js_info))
#endif


#define	jftchar_entry	wlchar_entry

struct jftfntinfo {
    struct bifont *jft_bf;
    int nfntchars;
    int width;
    int height;
    int depth;
    struct jftchar_entry ch[1];	/* character information	*/
};

#ifdef ANSI
#define	jftfinfo(fe)	(*(struct jftfntinfo **)&(fe->finfo))
#else
#define	jftfinfo(fe)	((struct jftfntinfo *)(fe->finfo))
#endif

#ifdef ANSI
#define	jsftfinfo(jsfi)	(*(struct jftfntinfo **)&(jsfi->js_info))
#else
#define	jsftfinfo(jsfi)	((struct jftfntinfo *)(jsfi->js_info))
#endif


#define	vflfntinfo	jftfntinfo
#define	vfl_bf		jft_bf
#define	vflchar_entry	jftchar_entry

#ifdef ANSI
#define	vflfinfo(fe)	(*(struct vflfntinfo **)&(fe->finfo))
#else
#define	vflfinfo(fe)	((struct vflfntinfo *)(fe->finfo))
#endif

#ifdef ANSI
#define	jsvflfinfo(jsfi)	(*(struct vflfntinfo **)&(jsfi->js_info))
#else
#define	jsvflfinfo(jsfi)	((struct vflfntinfo *)(jsfi->js_info))
#endif
#endif


/* struct to share subfont information
 */
#define	JSS_CLOSED	0
#define	JSS_INIT	1
#define	JSS_READ	2

struct jsubshare {
    int jss_stat;
    struct bifont *jss_bf;
    int jss_s;
    int jss_dev_font;		/* jstfm */
    struct jssinfo *jss_info;	/* jstfm wadalab */
    struct jsubshare *jss_next;
};


/* used to pass information from access to initfontinfo */
struct biaccessinfo {
    struct bifont *bf;
    short jsubf;
};

/* used to pass marking information from initfontinfo to readfontinfo */
struct biinitfontinfo {
    struct bifont *bf;
    int maxc;
    Boolean mark[1];
};

#ifdef ANSI
#define	biaccinfo(fe)	(*(struct biaccessinfo **)&(fe->finfo))
#define	biinifinfo(fe)	(*(struct biinitfontinfo **)&(fe->finfo))
#define	bfinfo(fe)	(*(struct bifont **)&(fe->finfo))
#else
#define	biaccinfo(fe)	((struct biaccessinfo *)(fe->finfo))
#define	biinifinfo(fe)	((struct biinitfontinfo *)(fe->finfo))
#define	bfinfo(fe)	((struct bifont *)(fe->finfo))
#endif


struct biinitfontinfo *alloc_biinif();


/*
 * Interface with device driver (only `non-standard' ones are listed)
 */
/* psbi.c */
int dev_is_tfm();
int dev_is_jstfm();
int dev_jstfm_kind();
int dev_getjsubfont();
int dev_is_jfm();
int dev_jfm_kind();
